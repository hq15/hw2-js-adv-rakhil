"use strict";
/* Примеры использования конструкция "try..catch":
 1. При получении даных с сервера в формате JSON и последующей обработкой их методом JSON.parse в случае если полученные данные будут не корректны то скрипт 'упадет' и об происшедшей ошибке мы можем узнать только открыв консоль, а использую try..catch мы имеем возможность предусмотреть какие-либо действия в таком случае, к примеру вывест сообщение на экран о не корректных данных.
 2. В случае с примером тех же данных о которых я писал выше, данные пришли корректные но в них не хватает какого-либо нужного нам свойства то без использования try..catch мы не обнаружим вовремя это не соотвествие. В таком случае можно восользоваться оператором throw который сгенерирует ошибку согласно указанного нами условия и позволит обработать эту ошибку в блоке catch (как в случае отсутствия необходимых свойств author, name, price в домашнем задании.
 Но "try..catch" - работает только с синтаксически чистым кодом.*/

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const list = document.createElement("ul");
const block = document.getElementById("root");
block.prepend(list);

const arrProperty = ['author', 'name', 'price'];

const listBooks = [];

createListBooks(books, arrProperty);
showListBooks(listBooks);

function showListBooks(arr) {
  const arrList = [];
  arr.forEach(function (item) {
    let stringBooks;
    stringBooks = `<li>Автор: ${item.author}, Название: "${item.name}", Цена: ${item.price} ГРН.</li>`;
    arrList.push(stringBooks);
  });
  list.insertAdjacentHTML("beforeend", `${arrList.join("")}`);
}

function createListBooks(arr, arrProperty) {
  arr.forEach(function (item, i) {
  try {
    arrProperty.forEach(function(property) {  
        if (!item.hasOwnProperty(property)) {
          throw  `Invalid Property ${property}, array index: ${i} not included in the array and will not be displayed on the page`;
        };
    });
    listBooks.push(item);
        return listBooks;
    } catch (error) {
      console.log(error);
    }
  });
}
